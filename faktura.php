<?php

class Faktura{
    public $brojRacuna;
    public $datum;
    public $organizacija;
    public $stavke=array();

    function __construct($brojRacuna,$datum,$organizacija)
    {
        $this->brojRacuna=$brojRacuna;
        $this->datum=$datum;
        $this->organizacija=$organizacija;
    }
    function dodajStavku($naziv,$kolicina){
        $this->stavke[]=new StavkaFakture($naziv,$kolicina);
    }

}
class StavkaFakture{
    public $naziv;
    public $kolicina;
    function __construct($naziv,$kolicina)
    {
        $this->naziv=$naziv;
        $this->kolicina=$kolicina;
    }

}
$faktura=null
?>